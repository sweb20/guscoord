$(function () {
    $('#sp_btn').on('click',function(e){
        $('.head_nav_sp').toggleClass('active');
        $('body').toggleClass('add_color');
        $('.sp_btn').toggleClass('close');
        e.stopPropagation();
    });
    $('.link_remove').on('click',function(e){
        $('.head_nav_sp').removeClass('active');
        $('body').removeClass('add_color');
        $('.sp_btn').removeClass('close');
        e.stopPropagation();
    });
    $(window).scroll(function(e) {
        if ($('.sp_btn').hasClass('close')) {
            $('.head_nav_sp').removeClass('active');
            $('body').removeClass('add_color');
            $('.sp_btn').removeClass('close');
        }
    });
    $(window).resize(function () {
        if ($('.sp_btn').hasClass('close')) {
            $('.head_nav_sp').removeClass('active');
            $('body').removeClass('add_color');
            $('.sp_btn').removeClass('close');
        }
    });
});

$(function(){
    $('a[href^="#"]').click(function(){
      var speed = 500;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $("html, body").animate({scrollTop:position}, speed, "swing");
      return false;
    });
});
